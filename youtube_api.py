from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

import pickle
from typing import TypedDict
class PlaylistDict(TypedDict):
    id: str
    title: str
    description: str

def get_credentials():
    try:
        # Load credentials from file
        with open('token.pickle', 'rb') as token:
            return pickle.load(token)
    except FileNotFoundError:
        # If no credentials file exists, create new credentials
        flow = InstalledAppFlow.from_client_secrets_file(
            'client_secret_vgusak.json', scopes=['https://www.googleapis.com/auth/youtube'])
        creds = flow.run_local_server()
        # Save credentials to file
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
        return creds

# Example usage:
creds = get_credentials()
# Load OAuth2 credentials from JSON file
youtube = build('youtube', 'v3', credentials=creds)

def get_title_of_youtube_playlisy_byid(id:str):
    playlist_response = youtube.playlists().list(
        part="snippet",
        id=id,
        fields="items(snippet(title))"
    ).execute()

    # Extract playlist title
    try:
        # Get the first item (should be the playlist itself)
        playlist_item = playlist_response["items"][0]
        playlist_title = playlist_item["snippet"]["title"]
        return playlist_item["snippet"]["title"]
        #print(f"Playlist Title: {playlist_title}")
    except IndexError:
        raise (f"Playlist with ID '{id}' not found.")
def get_channel_id():
    channel_request = youtube.channels().list(
        part='id',
        mine=True
    )
    channel_response = channel_request.execute()
    return channel_response['items'][0]['id']
def get_list_playlists(maxResults=200):# Adjust as needed
    playlists = []
    nextPageToken = None
    try:
        while True:
            request = youtube.playlists().list(
                part="snippet",
                mine=True,
                maxResults=50,
                pageToken=nextPageToken,
            )
            response = request.execute()
            playlists.extend(response['items'])
            nextPageToken = response.get('nextPageToken')
            if not nextPageToken or len(playlists)>maxResults:
                break

    except HttpError as e:
        print("An HTTP error {} occurred:\n{}".format(e.resp.status, e.content))
        return None

    return [{"id":response['id'], "title":response['snippet']['title'],"description":response['snippet']['description']}  for response in playlists]

def create_playlist(title, description):
    try:
        # Create a new playlist
        playlist = youtube.playlists().insert(
            part="snippet,status",
            body={
                "snippet": {
                    "title": title,
                    "description": description
                },
                "status": {
                    "privacyStatus": "public"  # Can be "private", "public" or "unlisted"
                }
            }
        ).execute()

        print("Playlist '{}' created successfully.".format(title))
        return playlist['id']

    except HttpError as e:
        print("An HTTP error {} occurred:\n{}".format(e.resp.status, e.content))
        return None

def add_video_to_playlist(playlist_id, video_id):
    try:
        # Add a video to the playlist
        youtube.playlistItems().insert(
            part="snippet",
            body={
                "snippet": {
                    "playlistId": playlist_id,
                    "resourceId": {
                        "kind": "youtube#video",
                        "videoId": video_id
                    }
                }
            }
        ).execute()

        print("Video '{}' added to playlist.".format(video_id))

    except HttpError as e:
        print("An HTTP error {} occurred:\n{}".format(e.resp.status, e.content))

def get_playlist_videos(playlist_id='PL2EmfHMCyF6wpkKiQj1USdnbEf_iLOOzn', max_results= 3):
    try:
        videos = []
        nextPageToken = None
        while True:
            # Retrieve list of videos in the playlist
            playlist_request = youtube.playlistItems().list(
                part="snippet",
                playlistId=playlist_id,
                maxResults=max_results,  # Adjust as needed
                pageToken=nextPageToken
            )
            playlist_response = playlist_request.execute()
            videos.extend(playlist_response['items'])
            nextPageToken = playlist_response.get('nextPageToken')
            if not nextPageToken or len(videos)>max_results:
                break

        return [{"id": video['snippet']['resourceId']['videoId'],"title": video['snippet']['title'],"description":video['snippet']['description']} for video in videos] #

    except HttpError as e:
        print("An HTTP error {} occurred:\n{}".format(e.resp.status, e.content))
        return None

if __name__=='__main__':
    pass

    #print(get_title_of_youtube_playlisy_byid('PLy7NrYWoggjwV7qC4kmgbgtFBsqkrsefG'))
    # cleared_playlist = [{"id":response['id'], "title":response['snippet']['title']}  for response in list_playlists()]
    # for p in cleared_playlist:
    #     print(p['title'])

    # videos = get_playlist_videos('PL2EmfHMCyF6y-_ue5iogwgSCwkRXaPqai');
    # for video in videos:
    #     print(video['title'])


    # if user_playlists:
    #     for playlist in user_playlists:
    #         print("Playlist ID: {}".format(playlist['id']))
    #         print("Title: {}".format(playlist['snippet']['title']))


    # playlist_id = 'PL2EmfHMCyF6wpkKiQj1USdnbEf_iLOOzn'
    # #playlist_id = create_playlist("My 2 New Playlist", "A playlist created programmatically.")
    # if playlist_id:
    #     add_video_to_playlist(playlist_id, "lnvAA3rhIo4" )

def get_watch_later():
    try:
        # Retrieve channel information
        channels_response = youtube.channels().list(part='contentDetails', mine=True).execute()
        if 'items' not in channels_response or len(channels_response['items']) == 0:
            print("No channel information found.")
            return None

        # Check if 'watchLater' playlist exists
        related_playlists = channels_response['items'][0]['contentDetails'].get('relatedPlaylists', {})
        if 'watchLater' not in related_playlists:
            print("No 'Watch Later' playlist found.")
            return None

        # Retrieve "Watch Later" playlist ID
        watch_later_playlist_id = related_playlists['watchLater']
        return watch_later_playlist_id

    except HttpError as e:
        print("An HTTP error {} occurred:\n{}".format(e.resp.status, e.content))
        return None

def get_watch_history_videos():
    try:
        videos = []
        nextPageToken = None
        while True:
            # Retrieve list of activities in the user's watch history

            history_request = youtube.activities().list(
                part="snippet,contentDetails",
                mine=False,
                maxResults=5,  # Adjust as needed
                pageToken=nextPageToken
            )
            history_response = history_request.execute()
            for item in history_response['items']:
                print((item))
                if item['snippet']['type'] == 'watch':
                    videos.append(item['contentDetails']['videoId'])
            nextPageToken = history_response.get('nextPageToken')
            if not nextPageToken:
                break

        return videos

    except Exception as e:
        print("An error occurred:", e)
        return None