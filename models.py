from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import String
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from dotenv import load_dotenv; import os; load_dotenv()
from sqlalchemy import create_engine
from sqlalchemy.orm import Session


TURSO_DATABASE_URL = os.environ.get("TURSO_DATABASE_URL")
TURSO_AUTH_TOKEN = os.environ.get("TURSO_AUTH_TOKEN")
dbUrl = f"sqlite+{TURSO_DATABASE_URL}/?authToken={TURSO_AUTH_TOKEN}&secure=true"
print(dbUrl)
engine = create_engine(dbUrl, connect_args={'check_same_thread': False}, echo=True)
#session = Session(engine)

class Base(DeclarativeBase):
    pass

class Foo(Base):
    __tablename__ = "foo"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    bar: Mapped[str] = mapped_column(String(100))
    def __repr__(self) -> str:
        return f"Item(id={self.id!r}, bar={self.bar!r})"

# Association table for many-to-many relationship between Playlist and Video
playlist_video_association = \
    (Table
     ('playlist_video', Base.metadata,
      Column('playlist_id', String, ForeignKey('playlist.id')),
      Column('video_id', String, ForeignKey('video.id'))
      ))
# Playlist model
class Playlist(Base):
    __tablename__ = 'playlist'

    id = Column(String, primary_key=True)
    title = Column(String)
    description = Column(String, default='')

    # Define the many-to-many relationship with Video
    videos = relationship('Video', secondary=playlist_video_association, backref='playlists')


# Video model
class Video(Base):
    __tablename__ = 'video'

    id = Column(String, primary_key=True)
    # video_id = Column(String, unique=True)
    title = Column(String)
    description = Column(String)


# Create tables
if __name__=='__main__':
    pass

    # Base.metadata.drop_all(engine)
    # Base.metadata.create_all(engine)
