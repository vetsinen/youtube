from playlist_db import get_downsynced_videos_for_playlist, downsync_playlist
from youtube_api import create_playlist, add_video_to_playlist, get_title_of_youtube_playlisy_byid, get_list_playlists
def upsync_videos_from_db( id:str, title: str, description: str=''):
    videos = get_downsynced_videos_for_playlist(id)
    if len(videos)==0:
        raise Exception(f'there is no playlist with id-{id} in db')
    playlist_id = create_playlist(title, description)
    for video in videos:
        print(video.id)
        add_video_to_playlist(playlist_id, video.id )

def clone_youtube_playlist(id:str, title=''):
    if title == '':
        title = "clone of: "+get_title_of_youtube_playlisy_byid(id)
    downsync_playlist({"id": id, "title": title, "description":""})

def downsync_user_playlists():
    playlists = get_list_playlists(maxResults=50)
    print(playlists[0])
    for playlist in playlists:
        downsync_playlist(playlist)

if __name__=='__main__':
    pass
    # downsync_user_playlists()
    clone_youtube_playlist('PL6bjPvAxWOH_HPr4lQwYjZtGqFaPAiY9r')
    # upsync_videos_from_db('PLcIcVZ3JhM-gQc62ueRnccCo43vd-donI', 'айті-початківці')
