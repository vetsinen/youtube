from models import Playlist, Video, engine, Session
from typing import Dict, List
from youtube_api import PlaylistDict, get_playlist_videos
from sqlalchemy.exc import IntegrityError

def downsync_playlist(playlist_dict: PlaylistDict):
    inserted_videos = 0
    with Session(engine) as session:
        # accessing or creating playlist in db
        playlist = session.get(Playlist, playlist_dict['id'])
        if not playlist:
            try:
                playlist = Playlist(id=playlist_dict['id'], title=playlist_dict['title'], description=playlist_dict['description'])
                session.add(playlist); session.commit()
            except Exception as e:
                print(type(e), str(e))
                raise Exception('imposible to get playlist object in db')

        videos_dict: List = get_playlist_videos(playlist_dict['id'], max_results=20)
        for video in videos_dict:
            try:
                dbvideo = Video(id=video['id'], title=video['title'], description= video['description'])
                playlist.videos.append(dbvideo)
                session.add(dbvideo)
                session.commit()
                inserted_videos = inserted_videos + 1
            except IntegrityError as e:
                print('record exists')
                session.rollback()
            except Exception as e:
                print(type(e), str(e))
                raise Exception('imposible to save video metadata in db')
    return inserted_videos

def get_downsynced_videos_for_playlist(id: str):
    with Session(engine) as session:
        playlist = session.get(Playlist, id)
        if not playlist:
            return None
        videos = playlist.videos
    return videos

if __name__=='__main__':
    print(len(get_downsynced_videos_for_playlist()))