import libsql_experimental as libsql
from dotenv import load_dotenv; import os; load_dotenv()
from sqlalchemy import create_engine



TURSO_DATABASE_URL = os.environ.get("TURSO_DATABASE_URL")
TURSO_AUTH_TOKEN = os.environ.get("TURSO_AUTH_TOKEN")

dbUrl = f"sqlite+{TURSO_DATABASE_URL}/?authToken={TURSO_AUTH_TOKEN}&secure=true"

engine = create_engine(dbUrl, connect_args={'check_same_thread': False}, echo=True)

# conn =url = os.getenv("TURSO_DATABASE_URL")
# auth_token = os.getenv("TURSO_AUTH_TOKEN"
# "youtube-manager.db", sync_url=url, auth_token=auth_token)
# conn.sync()
# conn.execute('''CREATE TABLE test2 (
#     id INTEGER PRIMARY KEY,
#     name TEXT
# );''')
# conn.commit(); conn.sync()
# conn.execute("INSERT INTO test2(name) VALUES ('abc');")
# conn.commit(); conn.sync()
# print(conn.execute("select * from test2").fetchall())