from sqlalchemy import select
from models import engine, Foo, Playlist, Video, engine, Session
from sqlalchemy.exc import IntegrityError
from playlist_db import downsync_playlist

# {'id': 'PL2EmfHMCyF6y-_ue5iogwgSCwkRXaPqai', 'title': 'солана', 'description': ''} {'id': 'PL2EmfHMCyF6z8EJxfyWkH_cA-sQn4pCu_', 'title': 'Àrchi', 'description': ''}
diverstyle = {'id': 'PLrSae6j9lpRLmCBMicUiunjUlYwoGYKYf', 'title': 'стиль от дивертиты', 'description': ''}

v = downsync_playlist(diverstyle);print(v); exit()

#session = Session(engine)
#playlist = session.get(Playlist, 'PL2EmfHMCyF6y-_ue5iogwgSCwkRXaPqai'); print(playlist.title); exit()

# Create a new Playlist
try:
    solana = Playlist(title='солана', id='PL2EmfHMCyF6y-_ue5iogwgSCwkRXaPqai', description='')
    session.add(solana);session.commit(); session.close()
except IntegrityError as e:
    print('already exists', type(e))

stmt = select(Video)
for item in session.scalars(stmt):
    print(item)

solana = session.get(Playlist, 'PL2EmfHMCyF6y-_ue5iogwgSCwkRXaPqai')

solana_videos= [{'id': 'Ts6-7Ae5QK4', 'title': 'ФІЄСТА SOLANA - забирай частку від $120,000 та безкоштовні NFT | Bybit Web3 x Solana Foundation'}, {'id': 'deC--b_Ir2Q', 'title': 'Что такое Solana? Обзор SOL с анимацией'}, {'id': '5FBCr43zPuc', 'title': 'Как создать Solana токен? Создаем свой токен на блокчейне солана без кода!'}, {'id': 'RIBjqKYhwik', 'title': 'Блокчейн протоколів Ethereum та Solana'}, {'id': 'OsJZkhtBU_o', 'title': '🟣 КАК создать СВОЙ ТОКЕН в блокчейне SOLANA | Техническое руководство #1'}, {'id': '5_ah_m4PvQM', 'title': 'Solcial - новости о социальной сети,обзор контента на площадке,отображение NFT'}, {'id': 'EbBUnr33fRg', 'title': '🔥Solcial: новостной блог - все плюсы и минусы'}, {'id': 'dRHICIj4FSg', 'title': 'ОБЗОР SOLANA | История проекта, Proof Of History, Принцип работы, Разработка под Солана'}, {'id': '8JUDx4JleO4', 'title': 'What Is Solcial (SLCL) and How Does It Monetize Web3 Social Content？'}]
# Create new Videos

for video in solana_videos:
    v = Video(id=video['id'], title = video['title'], description='')
    solana.videos.append(v)
    session.add(v)

session.commit()

# Close the session
session.close()